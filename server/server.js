const express = require('express');
const app = express();
const data = require('./data/data.json');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.get('/', (req, res) => {
    res.send(data)
});

app.listen(3000, () => {
    console.log('\x1b[36m%s\x1b[0m', 'Essence FE test server running on', 'http://localhost:3000');
});