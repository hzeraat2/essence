import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterText'
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        let sText = searchText.toLowerCase();
        if (sText.match(/\d+/g)) {
            sText = sText.substr(5, sText.length);
        }

        return items.filter(el => {
            return el.name.toLowerCase().includes(sText);
        });
    }
}
