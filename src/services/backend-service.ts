import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import { env } from "../environments/environment.dev";
import 'rxjs/add/operator/map';

@Injectable()
export class BackendService {

    constructor(private http: HttpClient) {
    }

    public query() {
        // const httpOptions = {headers: new HttpHeaders({ 'Access-Control-Allow-Origin':'*'})}

        return this.http.get(`${env.API_URL}`).pipe(map(
            response => {
                response => response.json()
                return response
            }))
    }
}