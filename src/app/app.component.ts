import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, Sort } from '@angular/material';
import { BackendService } from '../services/backend-service';
import { DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'Essence app';
  static tabledata: any;
  dataSource: any;
  sortedData;

  constructor(private backendService: BackendService) {

    this.backendService.query().subscribe(
      response => {
        console.log(response);
        AppComponent.tabledata = response;
        this.dataSource = new MatTableDataSource(AppComponent.tabledata);
        this.dataSource.paginator = this.paginator;

        this.sortedData = AppComponent.tabledata.slice();
      },
      error => {
        console.warn("An error occured...");
        console.warn(error);
      })
  }

  displayedColumns = ['code', 'make', 'model', 'name', 'tar_code', 'tar_data', 'tar_minutes', 'tar_name', 'tar_sms', 'type'];


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() { }

  ngAfterViewInit() { }

  sortData(sort: Sort) {
    const data = AppComponent.tabledata.slice();
    if (!sort.active || sort.direction == '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      let isAsc = sort.direction == 'asc';
      switch (sort.active) {
        case 'code': return compare(a.code, b.code, isAsc);
        case 'make': return compare(+a.make, +b.make, isAsc);
        case 'model': return compare(+a.model, +b.model, isAsc);
        case 'name': return compare(+a.name, +b.name, isAsc);
        case 'tar_code': return compare(+a.tar_code, +b.tar_code, isAsc);
        case 'tar_data': return compare(+a.tar_data, +b.tar_data, isAsc);
        case 'tar_minutes': return compare(+a.tar_minutes, +b.tar_minutes, isAsc);
        case 'tar_name': return compare(+a.tar_name, +b.tar_name, isAsc);
        case 'tar_sms': return compare(+a.tar_sms, +b.tar_sms, isAsc);
        case 'type': return compare(+a.type, +b.type, isAsc);
        default: return 0;
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.dataSource.sort = this.sort;
  }
}

export interface Element {
  code: string;
  make: string;
  model: string;
  name: string;
  tar_code: string;
  tar_data: string;
  tar_minutes: string;
  tar_name: string;
  tar_sms: string;
  type: string;
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

