# Essence Application

## Server side instructions below 
Open folder named `server` inside `application` folder 
Open terminal and run: `node server.js` as a command line
server will run on http://localhost:3000/

original code has been modified to cater for cross origin issues

new code:

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

# App use:

1 - Enter phrases in the filter input area to filter available plans
2 - Doubleclick / click the data table headers to sort that specific data table column adscending / descending 
3 - use the paginator to adjust the number of items shown on page or flick through pages

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Development server

Run `npm start` which is configured to Dev environment. Other environments with other end points can be configured teh same way

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Note: Due to time constraints unit tests have not been completed

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Note: This is left as well

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## For details on the new Angular Material visit below address
https://material.angular.io/
